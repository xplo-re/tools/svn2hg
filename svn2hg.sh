#!/bin/sh
# Copyright (c) 2014, xplo.re IT Services, Michael Maier.
# All rights reserved.

if test -z "$1" -o -z "$2";
then
	echo "usage: `basename "$0"` local-name URL"
	exit -1
fi

LOCAL="$1-mirror"
TARGET="$1"
REMOTE="$2"

die()
{
	echo "error: $1" >&2
	exit 1
}

# Canonicalises path by resolving symbolic links and return absolute path.
resolve_path()
{
	local resolved="$1"
	local target

	if test '.' = "$(dirname "$resolved")";
	then
		resolved="$(pwd)/$resolved"
	fi

	while test -L "$resolved";
	do
		target=$(expr "`ls -ld "$resolved"`" : '.*-> \(.*\)$')

		if expr "$target" : '/.*' >/dev/null;
		then
			resolved="$target"
		else
			# Prepend path information for link within same directory.
			resolved="$(dirname "$resolved")/$target"
		fi
	done

	echo "$resolved"
}

# Create local mirror repository.
svnadmin create --compatible-version 1.7 "$LOCAL" || die "failed to create local mirror repository '$LOCAL'"

# Use plain password file authentication and add svnsync user.
echo "
[general]
password-db = passwd
" >>"$LOCAL/conf/svnserve.conf" || die "failed to activate password authentication for local mirror '$LOCAL'"

echo "
[users]
svnsync = svnsync
" >>"$LOCAL/conf/passwd" || die "failed to add synchronisation user to local mirror '$LOCAL'"

echo "
[/]
svnsync = rw
" >>"$LOCAL/conf/authz" || die "failed to set permissions for synchronisation user on local mirror '$LOCAL'"

echo "#!/bin/sh
exit 0
" >"$LOCAL/hooks/pre-revprop-change" || die "failed to create revprop-change hook on local mirror '$LOCAL'"
chmod +x "$LOCAL/hooks/pre-revprop-change" || die "failed to set executable flag for revprop-change hook on local mirror '$LOCAL'"

# Init local mirror.
LOCAL_URL="file://`resolve_path "$LOCAL"`"
svnsync init "$LOCAL_URL" "$REMOTE" || die "failed to initialise synchronisation '$REMOTE' -> '$LOCAL'"

# Synchronise.
svnsync sync "$LOCAL_URL" || die "synchronisation failed for '$REMOTE' -> '$LOCAL'"

# Convert to Mercurial.
hg convert --authormap authors.map --source-type svn "$LOCAL" "$TARGET" || die "failed to convert to Mercurial"
cd "$TARGET" && hg update
